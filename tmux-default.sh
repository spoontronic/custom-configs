#!/bin/sh 
# launch tmux window with 3 panes

tmux new-session -d -s start 
tmux send-keys
tmux rename-window 'Start'
tmux split-window -h 'exec htop'
tmux send-keys
tmux split-window -v 
tmux -2 attach-session -t start

