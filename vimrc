"""Vim Customization
 
"Automatic installation of vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Plugin section
call plug#begin()
"Plugins
Plug 'vim-airline/vim-airline'
Plug 'preservim/nerdtree'
"Color schemes
Plug 'bluz71/vim-moonfly-colors'
Plug 'sainnhe/edge'
call plug#end()

"Disable compatibility with vi which can cause unexpected issues.
set nocompatible

"Enable type file detection. Vim will be able to try to detect the type of file in use.
filetype on

"Enable plugins and load plugin for the detected file type.
filetype plugin on

"Load an indent file for the detected file type.
filetype indent on

"Turn syntax highlighting on.
syntax on

"Add numbers to each line on the left-hand side.
set number

" While searching though a file incrementally highlight matching characters as you type.
set incsearch

"Set color scheme
if has('termguicolors')
  set termguicolors
endif

"Configuration options of `colorscheme edge`.
let g:edge_style = 'aura'
let g:edge_better_performance = 1
let g:airline_theme = 'edge'
let g:edge_transparent_background = 2

colorscheme edge

"Automatically handle indentation (PEP-8 standard) 
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

