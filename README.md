**vimrc**
```
wget https://gitlab.com/spoontronic/custom-vimrc/-/raw/main/vimrc && mv vimrc ~/.vimrc
```

**tmux-default.sh**
```
wget https://gitlab.com/spoontronic/custom-configs/-/raw/main/tmux-default.sh && sh tmux-default.sh
```

…
